﻿using GT.Entities;
using GT.ViewModel._Catalogos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GT.BL
{
    public class Mapeado
    {
        public void InicializacionEstatica()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                //Categoria
                cfg.CreateMap<VMCat_Categoria, GT_Cat_Categoria>();
                cfg.CreateMap<GT_Cat_Categoria, VMCat_Categoria>();


            });
        }
    }
}
