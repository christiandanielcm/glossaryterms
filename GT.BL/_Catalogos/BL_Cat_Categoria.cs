﻿using GT.DAO;
using GT.DAO.EF;
using GT.Entities;
using GT.ViewModel._Catalogos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GT.BL._Catalogos
{
    public class BL_Cat_Categoria
    {
        #region Categoria 

        public List<VMCat_Categoria> ListarCategorias(bool todos)
        {
            try
            {
                IDAOCrud<GT_Cat_Categoria> idao = new DAOCatCategoria();
                return AutoMapper.Mapper.Map<List<VMCat_Categoria>>(idao.Lista(todos));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public VMCat_Categoria ObtenerInfoCategoria(int id)
        {
            try
            {
                IDAOCrud<GT_Cat_Categoria> idao = new DAOCatCategoria();
                GT_Cat_Categoria res = idao.Obtener(id);
                return AutoMapper.Mapper.Map<VMCat_Categoria>(res);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public VMCat_Categoria RegistrarCategoria(VMCat_Categoria categoria)
        {
            try
            {
                IDAOCrud<GT_Cat_Categoria> idao = new DAOCatCategoria();
                GT_Cat_Categoria res = idao.Registrar(AutoMapper.Mapper.Map<GT_Cat_Categoria>(categoria));
                return AutoMapper.Mapper.Map<VMCat_Categoria>(res);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EditarCategoria(VMCat_Categoria categoria)
        {
            try
            {
                IDAOCrud<GT_Cat_Categoria> idao = new DAOCatCategoria();
                idao.Actualizar(AutoMapper.Mapper.Map<GT_Cat_Categoria>(categoria));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EliminarCategoria(short id)
        {
            try
            {
                IDAOCrud<GT_Cat_Categoria> idao = new DAOCatCategoria();
                idao.Eliminar(new GT_Cat_Categoria() { FiId = id });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BajaCategoria(short id)
        {
            try
            {
                IDAOCrud<GT_Cat_Categoria> idao = new DAOCatCategoria();
                idao.Baja(AutoMapper.Mapper.Map<GT_Cat_Categoria>(this.ObtenerInfoCategoria(id)));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

    }
}
