﻿using GT.ViewModel._Catalogos;
using GT.ViewModel._Excepciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace GT.Contratos
{
    [ServiceContract]
    public interface IServiceCatalogos
    {
        #region Categoria

        [OperationContract]
        [FaultContract(typeof(ExceptionService))]
        List<VMCat_Categoria> ListarCategorias(bool todas);

        [OperationContract]
        [FaultContract(typeof(ExceptionService))]
        VMCat_Categoria ObtenerInfoCategoria(short id);

        [OperationContract]
        [FaultContract(typeof(ExceptionService))]
        VMCat_Categoria RegistrarCategoria(VMCat_Categoria modelo);

        [OperationContract]
        [FaultContract(typeof(ExceptionService))]
        void EditarCategoria(VMCat_Categoria modelo);

        [OperationContract]
        [FaultContract(typeof(ExceptionService))]
        void EliminarCategoria(short id);

        [OperationContract]
        [FaultContract(typeof(ExceptionService))]
        void BajaCategoria(short id);

        #endregion 
    }
}
