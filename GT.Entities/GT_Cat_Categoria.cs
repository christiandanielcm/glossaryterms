namespace GT.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("GT_Cat_Categoria")]
    public partial class GT_Cat_Categoria
    {
        [Key]
        [Column("FiId", Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public short FiId { get; set; }

        [Column("FcNombre")]
        [StringLength(150)]
        public string FcNombre { get; set; }

        [Column("FcDescripcion")]
        [StringLength(250)]
        public string FcDescripcion { get; set; }

        [Key]
        [Column("FlActivo", Order = 1)]
        public bool FlActivo { get; set; }
    }
}
