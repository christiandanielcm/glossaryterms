namespace GT.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class GT_Cat_Termino
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short FiId { get; set; }

        [StringLength(150)]
        public string FcNombre { get; set; }

        public string FcDescripcion { get; set; }

        public string FcPathImage { get; set; }

        [Key]
        [Column(Order = 1)]
        public bool FlActivo { get; set; }
    }
}
