namespace GT.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    public partial class GT_Cat_Etiqueta
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short FiId { get; set; }

        [StringLength(100)]
        public string FcNombre { get; set; }
    }
}
