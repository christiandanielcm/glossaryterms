﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GT.ViewModel._Catalogos
{
    [DataContract]
   public class VMCat_Categoria
    {
        private short fiId;
        private string fcNombre;
        private string fcDescripcion;
        private bool flActivo;

        [DataMember]
        public short FiId { get => fiId; set => fiId = value; }
        [DataMember]
        public string FcNombre { get => fcNombre; set => fcNombre = value; }
        [DataMember]
        public string FcDescripcion { get => fcDescripcion; set => fcDescripcion = value; }
        [DataMember]
        public bool FlActivo { get => flActivo; set => flActivo = value; }

    }
}
