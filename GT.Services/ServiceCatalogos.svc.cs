﻿using GT.BL._Catalogos;
using GT.Contratos;
using GT.ViewModel._Catalogos;
using GT.ViewModel._Excepciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Utilidades;

namespace GT.Services
{
   
    public class ServiceCatalogos : IServiceCatalogos
    {

       
        public void BajaCategoria(short id)
        {
            try
            {
                new BL_Cat_Categoria().BajaCategoria(id);
            }
            catch(Exception ex)
            {
                ExceptionService exception = new ExceptionService()
                {
                    Mensaje = new Excepciones().ObtenerMsjExcepcion(ex),
                    Operacion = "Baja Categoria"
                };
                throw new FaultException<ExceptionService>(exception);
            }
        }

        public void EditarCategoria(VMCat_Categoria modelo)
        {
            try
            {
                new BL_Cat_Categoria().EditarCategoria(modelo);
            }
            catch (Exception ex)
            {
                ExceptionService exception = new ExceptionService()
                {
                    Mensaje = new Excepciones().ObtenerMsjExcepcion(ex),
                    Operacion = "Editar Categoria"
                };
                throw new FaultException<ExceptionService>(exception);
            }
        }

        public void EliminarCategoria(short id)
        {
            try
            {
                new BL_Cat_Categoria().EliminarCategoria(id);
            }
            catch (Exception ex)
            {
                ExceptionService exception = new ExceptionService()
                {
                    Mensaje = new Excepciones().ObtenerMsjExcepcion(ex),
                    Operacion = "Eliminar Categoria"
                };
                throw new FaultException<ExceptionService>(exception);
            }
        }

        public List<VMCat_Categoria> ListarCategorias(bool todas)
        {
            try
            {
                return new BL_Cat_Categoria().ListarCategorias(todas);
            }
            catch (Exception ex)
            {
                ExceptionService exception = new ExceptionService()
                {
                    Mensaje = new Excepciones().ObtenerMsjExcepcion(ex),
                    Operacion = "Listar Categorias"
                };
                throw new FaultException<ExceptionService>(exception);
            }

        }

        public VMCat_Categoria ObtenerInfoCategoria(short id)
        {
            try
            {
                return new BL_Cat_Categoria().ObtenerInfoCategoria(id);
            }
            catch (Exception ex)
            {
                ExceptionService exception = new ExceptionService()
                {
                    Mensaje = new Excepciones().ObtenerMsjExcepcion(ex),
                    Operacion = "Obtener Info Categoria"
                };
                throw new FaultException<ExceptionService>(exception);
            }
        }

        public VMCat_Categoria RegistrarCategoria(VMCat_Categoria modelo)
        {
            try
            {
               return new BL_Cat_Categoria().RegistrarCategoria(modelo);
            }
            catch (Exception ex)
            {
                ExceptionService exception = new ExceptionService()
                {
                    Mensaje = new Excepciones().ObtenerMsjExcepcion(ex),
                    Operacion = "Registrar Categoria"
                };
                throw new FaultException<ExceptionService>(exception);
            }
        }
    }
}
