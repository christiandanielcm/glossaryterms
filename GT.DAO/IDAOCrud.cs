﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GT.DAO
{
    public interface IDAOCrud<T>
    {
        T Registrar(T entidad);
        T Obtener(params object[] ids);
        void Actualizar(T entidad);
        void Eliminar(T entidad);
        void Baja(T entidad);
        //List<T> Listar(Grid grid);
        List<T> Lista(bool todos);
        int Total();
    }
}
