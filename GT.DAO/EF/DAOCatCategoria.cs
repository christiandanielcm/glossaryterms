﻿using GT.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilidades;

namespace GT.DAO.EF
{
    public class DAOCatCategoria : IDAOCrud<GT_Cat_Categoria>
    {
        private Excepciones excepciones = null;

        public DAOCatCategoria()
        {
            excepciones = new Excepciones();
        }

        public void Actualizar(GT_Cat_Categoria categoria)
        {
            using (dbGTContext con = new dbGTContext())
            {
                con.Entry(categoria).State = System.Data.Entity.EntityState.Modified;
                //con.Entry(categoria).Property(y => y.FiId).IsModified = false;
                con.Entry(categoria).Property(y => y.FcNombre).IsModified = true;
                con.Entry(categoria).Property(y => y.FcDescripcion).IsModified = true;
                con.Entry(categoria).Property(y => y.FlActivo).IsModified = true;
                con.SaveChanges();
            }
        }

        public void Baja(GT_Cat_Categoria categoria)
        {
            try
            {
                using (dbGTContext con = new dbGTContext())
                {
                    con.Entry(categoria).State = System.Data.Entity.EntityState.Modified;
                    categoria.FlActivo = false;
                    con.SaveChanges();
                }
            }
            catch (DbEntityValidationException ex)
            {
                throw new Exception(excepciones.ValidarEntityException(ex));
            }
        }

        public void Eliminar(GT_Cat_Categoria categoria)
        {
            using (dbGTContext con = new dbGTContext())
            {
                con.Entry(categoria).State = System.Data.Entity.EntityState.Deleted;
                con.SaveChanges();
            }
        }

        public List<GT_Cat_Categoria> Lista(bool todos)
        {
            using (dbGTContext con = new dbGTContext())
            {
                List<GT_Cat_Categoria> lista = new List<GT_Cat_Categoria>();
                lista = con.GT_Cat_Categoria
                    .Where(c => (todos) || (c.FlActivo && !todos))
                    .ToList();
                return lista;
            }
        }

        public GT_Cat_Categoria Obtener(params object[] ids)
        {
            using (dbGTContext con = new dbGTContext())
            {
                short id = short.Parse(ids[0].ToString());
                return con.GT_Cat_Categoria
                    .Where(x => x.FiId == id)
                    .FirstOrDefault();
            }
        }

        public GT_Cat_Categoria Registrar(GT_Cat_Categoria categoria)
        {
            try
            {
                //if (categoria.FiId != 0)
                //{
                    using (dbGTContext con = new dbGTContext())
                    {
                        con.Entry(categoria).State = System.Data.Entity.EntityState.Added;
                        con.SaveChanges();
                        return categoria;
                    }
                //}
                //else
                //{
                //    throw new DbEntityValidationException("Se debe indicar un id para la categoria.");
                //}
            }
            catch (DbEntityValidationException ex)
            {
                throw new Exception(excepciones.ValidarEntityException(ex));
            }
        }

        public int Total()
        {
            throw new NotImplementedException();
        }
    }
}
