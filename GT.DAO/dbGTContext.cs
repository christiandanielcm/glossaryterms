namespace GT.DAO
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using GT.Entities;

    public partial class dbGTContext : DbContext
    {
        public dbGTContext()
            : base("name=dbGTContext")
        {
        }

        public virtual DbSet<GT_Cat_Categoria> GT_Cat_Categoria { get; set; }
        public virtual DbSet<GT_Cat_Etiqueta> GT_Cat_Etiqueta { get; set; }
        public virtual DbSet<GT_Cat_Termino> GT_Cat_Termino { get; set; }
        public virtual DbSet<GT_Rel_CategoriaTermino> GT_Rel_CategoriaTermino { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
